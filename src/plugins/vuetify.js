import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import i18n from '@/i18n'

Vue.use(Vuetify)

const theme = {
  primary: '#04080F',
  secondary: '#507DBC',
  accent: '#A1C6EA',
  info: '#BBD1EA',
  warning: '#5F9EA0',
  success: '#44BBA4',
}

export default new Vuetify({
  lang: {
    t: (key, ...params) => i18n.t(key, params),
  },
  theme: {
    themes: {
      dark: theme,
      light: theme,
    },
  },
})
